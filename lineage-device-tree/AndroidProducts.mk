#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_addison.mk

COMMON_LUNCH_CHOICES := \
    lineage_addison-user \
    lineage_addison-userdebug \
    lineage_addison-eng
